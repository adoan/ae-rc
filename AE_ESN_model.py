import tensorflow as tf
tf.keras.backend.set_floatx('float32')

from tensorflow.keras import Input
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, UpSampling2D, concatenate, BatchNormalization, Conv2DTranspose, Flatten, PReLU, Reshape, Dropout, AveragePooling2D, add, Lambda, Layer, TimeDistributed, LSTM
from tensorflow.keras.regularizers import l2
from tensorflow.keras.initializers import Constant
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Model
from ESN_tools import *

from tensorflow.keras.callbacks import ModelCheckpoint,EarlyStopping

#from keras.backend import tensorflow as tf
#import keras as Kr
import numpy as np

class Autoencoder():
    def __init__(self,Nx,Nu,features_layers=[1],latent_dim=16,
        filter_window=(3,3),act_fct='tanh',batch_norm=False,
        drop_rate=0.0, lmb=0.0,resize_meth='bilinear'):

        self.Nx = Nx
        self.Nu = Nu
        self.features_layers = features_layers
        self.latent_dim = latent_dim
        self.filter_window = filter_window
        self.act_fct = act_fct
        self.batch_norm = batch_norm
        self.drop_rate = drop_rate
        self.lmb =lmb
        self.resize_meth = resize_meth
        self.encoder = Encoder(self.Nx,self.Nu,self.features_layers,
            self.latent_dim,self.filter_window,self.act_fct,
            self.batch_norm,self.drop_rate,self.lmb)
        self.decoder = Decoder(self.Nx,self.Nu,self.features_layers,
            self.latent_dim,self.filter_window,self.act_fct,self.batch_norm,
            self.drop_rate,self.lmb,self.resize_meth)

    @tf.function
    def build_ae(self,inputs):
        self.input_ae = inputs
        self.encoded = self.encoder(self.input_ae)
        self.decoded = self.decoder(self.encoded)
        self.autoencoder = Model(self.input_ae,outputs=self.decoded)

    @tf.function
    def compile_ae(self,learning_rate,loss):
        self.autoencoder.compile(optimizer=Adam(lr=learning_rate),loss=loss)

    @tf.function
    def save(fln):
        self.autoencoder.save(fln)

    @tf.function
    def save_weights(fln):
        self.autoencoder.save_weights(fln)

    @tf.function
    def load(fln):
        self.autoencoder = load_model(fln)

    @tf.function
    def load_weights(fln):
        self.autoencoder.load_weights

    @tf.function
    def train(self,inputs,targets,inputs_valid,targets_valid,
        nb_epoch,batch_size,
        learning_rate_list,pat, tempfn):
        model_cb=ModelCheckpoint(tempfn, monitor='val_loss',save_best_only=True,verbose=1)
        early_cb=EarlyStopping(monitor='val_loss', patience=pat,verbose=1)
        cb = [model_cb, early_cb]
        self.train_history = []
        self.val_history = []
        for i in range(len(learning_rate_list)):
            learning_rate = learning_rate_list[i]
            K.set_value(self.autoencoder.optimizer.lr,learning_rate)
            hist0 = self.autoencoder.fit(inputs,targets,
                        epochs=nb_epoch,
                        batch_size=batch_size,
                        shuffle=True,
                        validation_data=(inputs_valid, targets_valid),
                        callbacks=cb)

            self.train_history.extend( hist0.history['loss'] )
            self.val_history.extend( hist0.history['val_loss'] )
        return self.train_history, self.val_history

class AE_ESN():
    def __init__(self,Nx,Nu,features_layers=[1],latent_dim=16,
        filter_window=(3,3),act_fct='tanh',batch_norm=False,
        drop_rate=0.0, lmb=0.0,resize_meth='bilinear',
        num_units=100, alpha=1.0,rho=0.6,sparseness=0.99,sigma_in=1.0,gamma=0.0001):

        self.Nx = Nx
        self.Nu = Nu
        self.features_layers = features_layers
        self.latent_dim = latent_dim
        self.filter_window = filter_window
        self.act_fct = act_fct
        self.batch_norm = batch_norm
        self.drop_rate = drop_rate
        self.lmb =lmb
        self.resize_meth = resize_meth

        self.encoder = Encoder(self.Nx,self.Nu,self.features_layers,
            self.latent_dim,self.filter_window,self.act_fct,
            self.batch_norm,self.drop_rate,self.lmb)

        self.input_code = Input(shape=(1,1,self.latent_dim))

        self.decoder = Decoder(self.Nx,self.Nu,self.features_layers,
            self.latent_dim,self.filter_window,self.act_fct,self.batch_norm,
            self.drop_rate,self.lmb,self.resize_meth)

        self.ESNlayer = tf.keras.layers.RNN(ESNCell(num_units, latent_dim, alpha, rho, sparseness,sigma_in),
                            return_sequences=True, name='ESN', stateful=True, return_state=True)
        self.ESNlayer.build((1,None,latent_dim))
        self.Wout = TimeDistributed(Dense(latent_dim,activation=None,kernel_regularizer=l2(gamma)))

    # @tf.function
    # def build_ae(self,inputs):
    #     self.input_ae = inputs
    #     self.encoded = self.encoder(self.input_ae)
    #     self.decoded = self.decoder(self.encoded)
    #     self.autoencoder = Model(self.input_ae,outputs=self.decoded)

    @tf.function
    def build_ae_esn(self,input_sequence):
        self.input_tae = input_sequence

        x = TimeDistributed(self.encoder.layers[0])(self.input_tae)
        for i in range(1,len(self.encoder.layers)):
            x = TimeDistributed(self.encoder.layers[i])(x)
        self.encoded_t = x
        # self.encoder_t = Model(self.input_tae,x)

        self.ESN_X,self.ESN_state = self.ESNlayer(self.encoded_t)
        self.ESN_Y = self.Wout(self.ESN_X)

        x = TimeDistributed(self.decoder.layers[0])(self.ESN_Y)
        for i in range(1,len(self.decoder.layers)):
            x = TimeDistributed(self.decoder.layers[i])(x)
        self.decoded_t = x
        # self.decoded_t = TimeDistributed( self.decoder) (self.ESN_Y)
        self.ae_esn = Model(self.input_tae,outputs=self.decoded_t)
        print(self.ae_esn.summary())
        # self.input_shape = Input(shape=(self.Nx,self.Nx,self.Nu))
        # self.code = self.encoder(self.input_shape)
        # self.coder_model = Model(self.input_shape,outputs=self.code)
        
        # self.decoded = self.decoder(self.input_code)
        # self.decoder_model = Model(self.input_code,outputs=self.decoded)

        # self.input_sequence = input_sequence #tf.keras.layers.Input((None,) + self.input_shape)
        # self.encoded_t = TimeDistributed( self.coder_model) (self.input_sequence)
        # self.ESN_X,self.ESN_state = self.ESNlayer(self.encoded_t)
        # self.ESN_Y = self.Wout(self.ESN_X)
        # self.decoded_t = TimeDistributed( self.decoder_model) (self.ESN_Y)
        # self.ae_esn = Model(self.input_sequence,outputs=self.decoded_t)


    def compile_ae_esn(self,learning_rate,loss):
        self.ae_esn.compile(optimizer=Adam(lr=learning_rate),loss=loss)

    def save(fln):
        self.ae_esn.save(fln)

    def save_weights(fln):
        self.ae_esn.save_weights(fln)

    def load(fln):
        self.ae_esn = load_model(fln)

    @tf.function
    def load_weights(fln):
        self.ae_esn.load_weights

    @tf.function
    def train(self,inputs,targets,inputs_valid,targets_valid,
        nb_epoch,batch_size,
        learning_rate_list,pat, tempfn):
        model_cb=ModelCheckpoint(tempfn, monitor='val_loss',save_best_only=True,verbose=1)
        early_cb=EarlyStopping(monitor='val_loss', patience=pat,verbose=1)
        cb = [model_cb, early_cb]
        self.train_history = []
        self.val_history = []
        for i in range(len(learning_rate_list)):
            learning_rate = learning_rate_list[i]
            K.set_value(self.ae_esn.optimizer.lr,learning_rate)
            hist0 = self.ae_esn.fit(inputs,targets,
                        epochs=nb_epoch,
                        batch_size=batch_size,
                        shuffle=True,
                        validation_data=(inputs_valid, targets_valid),
                        callbacks=cb,
                        steps_per_epoch=1)

            self.train_history.extend( hist0.history['loss'] )
            self.val_history.extend( hist0.history['val_loss'] )
        return self.train_history, self.val_history


# Coder branch of an AE as a Layer
class Encoder(Layer):
    def __init__(self,Nx,Nu,features_layers=[1],latent_dim=16,
        filter_window=(3,3),act_fct='tanh',batch_norm=False,drop_rate=0.0, lmb=0.0, **kwargs):
        super(Encoder,self).__init__(**kwargs)
        self.Nx = Nx
        self.Nu = Nu
        self.features_layers = features_layers
        self.latent_dim = latent_dim
        self.filter_window = filter_window
        self.act_fct = act_fct
        self.batch_norm = batch_norm
        self.drop_rate = drop_rate
        self.lmb =lmb
        
        # self.Input = Input(batch_shape = (None,Nx,Nx,Nu))
        self.layers = []
        self.layers.append( Conv2D(self.features_layers[0],self.filter_window, padding='same',kernel_regularizer=l2(self.lmb), bias_regularizer=l2(self.lmb), activation=self.act_fct,input_shape=(None,self.Nx,self.Nx,self.Nu))  )
        for i in range(1,len(features_layers)):
            self.layers.append( Conv2D(self.features_layers[i],self.filter_window, padding='same',kernel_regularizer=l2(self.lmb), bias_regularizer=l2(self.lmb), activation=self.act_fct)  )
            
            if (self.batch_norm):
                self.layers.append( BatchNormalization() )
            
            self.layers.append( MaxPool2D((2,2), padding='same') )

            self.layers.append( Dropout(drop_rate) )

        self.layers.append( Flatten() )

        self.layers.append(Dense(self.latent_dim,kernel_regularizer=l2(self.lmb), bias_regularizer=l2(self.lmb),activation=self.act_fct) )

        self.layers.append( Dropout(drop_rate) )

        self.nb_layer = len(self.layers)

    def call(self, inputs):
        x = self.layers[0](inputs)
        for i in range(1,len(self.layers)):
            x = self.layers[i](x)
        return x

    def build(self, input_shape):
        super(Encoder, self).build(input_shape)

    def get_config(self):
        base_config = super().get_config()
        config = {'Nx':self.Nx,'Nu':self.Nu,
            'features_layers':self.features_layers,
            'latent_dim':self.latent_dim,'filter_window':self.filter_window,
            'act_fct':self.act_fct,'batch_norm':self.batch_norm,'drop_rate':self.drop_rate,'lmb':self.lmb}
        # base_config = super(Encoder, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

class Decoder(Layer):
    def __init__(self,Nx,Nu,features_layers=[1],latent_dim=16,
        filter_window=(3,3),act_fct='tanh',batch_norm=False,drop_rate=0.0, lmb=0.0,resize_meth='bilinear', **kwargs):
        super(Decoder,self).__init__(**kwargs)
        self.Nx = Nx
        self.Nu = Nu
        self.features_layers = features_layers
        self.latent_dim = latent_dim
        self.filter_window = filter_window
        self.act_fct = act_fct
        self.batch_norm = batch_norm
        self.drop_rate = drop_rate
        self.lmb =lmb
        self.resize_meth = resize_meth
        if self.resize_meth=='bicubic':
            resize_method = tf.image.ResizeMethod.BICUBIC
        else:
            resize_method = tf.image.ResizeMethod.BILINEAR

        self.prelatent_Nx = int(Nx/ (2**len(features_layers)))

        self.prelatent_shape = self.features_layers[-1] * (Nx/2.**len(features_layers)) * (Nx/2.**len(features_layers))
        # self.Input = Input(batch_shape = (None,Nx,Nx,Nu))

        self.nb_layer = 0
        self.layers = []
        self.layers.append( Dense(self.prelatent_shape,kernel_regularizer=l2(self.lmb), bias_regularizer=l2(self.lmb), activation=self.act_fct) )
        self.nb_layer = self.nb_layer+1

        self.layers.append( BatchNormalization() )
        self.nb_layer = self.nb_layer+1
        
        self.layers.append( Reshape((self.prelatent_Nx,self.prelatent_Nx,features_layers[-1])) )

        for i in range(len(features_layers)-1):
            # self.layers.append( ResizeImages( (int(self.prelatent_Nx*(2**(i+1))), int(self.prelatent_Nx*(2**(i+1)))),resize_meth) )
            self.layers.append( UpSampling2D( size=(2,2), interpolation='bilinear') )
            self.layers.append( Conv2D(self.features_layers[-i-2],self.filter_window, padding='same',kernel_regularizer=l2(self.lmb), bias_regularizer=l2(self.lmb), activation=self.act_fct)  )
            
            if (self.batch_norm):
                self.layers.append(  BatchNormalization() )

            self.layers.append( Dropout(drop_rate) )

        # self.layers.append( ResizeImages((self.Nx,self.Nx),resize_meth) )
        self.layers.append( UpSampling2D( size=(2,2), interpolation='bilinear') )

        self.layers.append( Conv2D(self.Nu,self.filter_window, padding='same',kernel_regularizer=l2(self.lmb), bias_regularizer=l2(self.lmb), activation='linear')  )


    def call(self, inputs):
        x = self.layers[0](inputs)
        for i in range(1,len(self.layers)):
            x = self.layers[i](x)
        return x

    def build(self, input_shape):
        super(Decoder, self).build(input_shape)

    def get_config(self):
        base_config = super().get_config()
        config = {'Nx':self.Nx,'Nu':self.Nu,
            'features_layers':self.features_layers,
            'latent_dim':self.latent_dim,'filter_window':self.filter_window,
            'act_fct':self.act_fct,'batch_norm':self.batch_norm,'drop_rate':self.drop_rate,'lmb':self.lmb,
            'resize_meth':self.resize_meth}
        return dict(list(base_config.items()) + list(config.items()) ) #dict(list(config.items()))
        #
        #return dict(list(base_config.items()) + list(config.items()) )

class ResizeImages(Layer):
    """Resize Images to a specified size

    # Arguments
        output_size: Size of output layer width and height
        data_format: A string,
            one of `channels_last` (default) or `channels_first`.
            The ordering of the dimensions in the inputs.
            `channels_last` corresponds to inputs with shape
            `(batch, height, width, channels)` while `channels_first`
            corresponds to inputs with shape
            `(batch, channels, height, width)`.
            It defaults to the `image_data_format` value found in your
            Keras config file at `~/.keras/keras.json`.
            If you never set it, then it will be "channels_last".

    # Input shape
        - If `data_format='channels_last'`:
            4D tensor with shape:
            `(batch_size, rows, cols, channels)`
        - If `data_format='channels_first'`:
            4D tensor with shape:
            `(batch_size, channels, rows, cols)`

    # Output shape
        - If `data_format='channels_last'`:
            4D tensor with shape:
            `(batch_size, pooled_rows, pooled_cols, channels)`
        - If `data_format='channels_first'`:
            4D tensor with shape:
            `(batch_size, channels, pooled_rows, pooled_cols)`
    """
    def __init__(self, output_dim=(1, 1),resize_meth='bilinear', **kwargs):
        self.output_dim = output_dim
        super(ResizeImages, self).__init__(**kwargs)
        #data_format = conv_utils.normalize_data_format(data_format)
        #self.output_dim = conv_utils.normalize_tuple(output_dim, 2, 'output_dim')
        #self.input_spec = InputSpec(ndim=4)
        self.resize_meth = resize_meth
        if resize_meth=='bicubic':
            self.resize_method = tf.image.ResizeMethod.BICUBIC
        else:
            self.resize_method = tf.image.ResizeMethod.BILINEAR

    def build(self, input_shape):
        super(ResizeImages, self).build(input_shape)
        #self.input_spec = [InputSpec(shape=input_shape)]

    def compute_output_shape(self, input_shape):
#        if self.data_format == 'channels_first':
#            return (input_shape[0], input_shape[1], self.output_dim[0], self.output_dim[1])
#        elif self.data_format == 'channels_last':
         return (input_shape[0], self.output_dim[0], self.output_dim[1], input_shape[3])

#    def _resize_fun(self, inputs, data_format):
#        try:
#            assert keras.backend.backend() == 'tensorflow'
#            assert self.data_format == 'channels_last'
#        except AssertionError:
#            print( "Only tensorflow backend is supported for the resize layer and accordingly 'channels_last' ordering")
#        output = tf.resize_images(inputs, self.output_dim,method=self.resize_method)
#        return output

    def call(self, inputs):
#        output = self._resize_fun(inputs=inputs, data_format=self.data_format)
        output = tf.image.resize(inputs, self.output_dim,method=self.resize_method)
        return output

    def get_config(self):
        config = {'output_dim': self.output_dim,
                'resize_meth':self.resize_meth}
        base_config = super(ResizeImages, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


def create_timedist_ae_esn_model(Nx,Ny, 
                                     num_units, num_inputs,
                                     alpha, rho, sparseness, sigma_in,
                                     Nu=1, stateful=True,
                        features_layers=[1], latent_dim=16,
                        resize_meth='bilinear', filter_window=(3,3), 
                        act_fct='tanh', batch_norm=False, 
                         drop_rate=0.0, lmb=0.0,rn_seed=1):
    rng = np.random.RandomState(rn_seed)
    input_shape = (Nx,Nx,Nu)
    input_img = Input(shape = input_shape)
    # if (stateful):
    #     input_img = Input(shape = input_shape) #, batch_size=1)
    # else:
    #     input_img = Input(shape = input_shape) #, batch_size=1)

    if resize_meth=='bicubic':
        resize_method = tf.image.ResizeMethod.BICUBIC
    else:
        resize_method = tf.image.ResizeMethod.BILINEAR

    ## ENCODER DEFINITION
    nb_layer = 0
    for i in range(len(features_layers)):
        if i==0:
            x =  Conv2D(features_layers[i],filter_window, padding='same',kernel_regularizer=l2(lmb), bias_regularizer=l2(lmb), activation=act_fct) (input_img)
            nb_layer = nb_layer+1
        else:
            x =   Conv2D(features_layers[i],filter_window, padding='same',kernel_regularizer=l2(lmb), bias_regularizer=l2(lmb), activation=act_fct) (x)
            nb_layer = nb_layer+1
        
        if batch_norm:
            x =  BatchNormalization()(x)
            nb_layer = nb_layer+1
        
        x =  MaxPool2D((2,2), padding='same') (x)
        nb_layer = nb_layer+1
        #x =  Dropout(drop_rate)(x)
        #nb_layer = nb_layer+1
    
    prelatent_shape = x.get_shape().as_list()
    prelatent_shape = prelatent_shape[-3:]
    last_size0 = prelatent_shape[-3]
    last_size1 = prelatent_shape[-2]
    
    x =  Flatten() (x)
    nb_layer = nb_layer+1
    x =  Dense(latent_dim,kernel_regularizer=l2(lmb), bias_regularizer=l2(lmb),activation=act_fct) (x)
    nb_layer = nb_layer+1
    encoded =  Dropout(drop_rate)(x)
    nb_layer = nb_layer+1

    encoder = Model(input_img,encoded)
    print(encoder.summary())


    ## DECODER DEFINITION
    encoded_shape = (encoded.shape[-1],)
    encoded_input = Input(shape=(encoded.shape[-1],))

    x =  Dense(np.prod(prelatent_shape),kernel_regularizer=l2(lmb), bias_regularizer=l2(lmb), activation=act_fct)(encoded_input)
    nb_layer = nb_layer+1

    if batch_norm:
        x =  BatchNormalization()(x)
        nb_layer = nb_layer+1
    
    x =  Dropout(drop_rate)(x)
    nb_layer = nb_layer+1
    
    x =  Reshape((last_size0,last_size1,features_layers[-1]))(x)
    nb_layer = nb_layer+1
    
    for i in range(len(features_layers)-1):
        size = x.get_shape().as_list()
        size = size[-3:]
        # x = Lambda(lambda x: tf.image.resize_images(x,(size[0]*2,size[1]*2),method=resize_method) ) (x)
        # x =  ResizeImages((size[0]*2,size[1]*2),resize_meth) (x)
        x = UpSampling2D(size=(2,2),interpolation='bilinear')(x)
        nb_layer = nb_layer+1
        x =  Conv2D(features_layers[-i-2],filter_window, padding='same',kernel_regularizer=l2(lmb), bias_regularizer=l2(lmb), activation=act_fct)(x)
        nb_layer = nb_layer+1
        
        if batch_norm:
            x =  BatchNormalization()(x)
            nb_layer = nb_layer+1
        
        #x =  Dropout(drop_rate) (x)
        #nb_layer = nb_layer+1
    
    size = x.get_shape().as_list()
    size = size[-3:]
    # x = Lambda(lambda x: tf.image.resize_images(x,(size[0]*2,size[1]*2),method=resize_method) ) (x)
    # x =  ResizeImages((size[0]*2,size[1]*2),resize_meth) (x)
    x = UpSampling2D(size=(2,2),interpolation='bilinear')(x)
    nb_layer = nb_layer+1
    decoded =  Conv2D(Nu,filter_window, padding='same',kernel_regularizer=l2(lmb), bias_regularizer=l2(lmb), activation='linear') (x)
    nb_layer = nb_layer+1
    
    decoder = Model(encoded_input,decoded)
    print(decoder.summary())

    ## AUTOENCODER DEFINITION
    x = encoder(input_img)
    y = decoder(x)
    autoencoder = Model(input_img,y)
    print(autoencoder.summary())

    ## TIME-DISTRIBUTED AUTOENCODER
    if stateful:
        input_seq = Input((None,) + input_shape, batch_size=1) ## CAREFUL HERE - NAKD
    else:
        input_seq = Input((None,) + input_shape) ## CAREFUL HERE - NAKD
    encoded_t = TimeDistributed(encoder)(input_seq)
    print(encoded_t.shape)

    ## ESN DEFINITION
    esn_layer = tf.keras.layers.RNN(ESNCell(num_units, num_inputs, alpha, rho, sparseness,sigma_in,rng=rng),
                            return_sequences=True, name='ESN', stateful=stateful, return_state=True)
    esn_layer.trainable = False
    if (stateful):
        esn_layer.build((1,None,num_inputs))

    x,ESN_state = esn_layer(encoded_t)
    nb_layer = nb_layer+1
    
    encoded_t =  TimeDistributed(Dense(num_inputs,activation=None,kernel_regularizer=l2(0.0001) ), name='Wout' )(x)
    nb_layer = nb_layer+1

    if stateful:
        encoded_seq = Input((None,) + encoded_shape, batch_size=1) ## CAREFUL HERE - NAKD
    else:
        encoded_seq = Input((None,) + encoded_shape) ## CAREFUL HERE - NAKD
    decoded_t = TimeDistributed(decoder)(encoded_t)

    ae_esn = Model(input_seq,decoded_t)
    ae_esn_st = Model(input_seq,outputs=[decoded_t,ESN_state])
    
    if (stateful):
        esn_input = Input(shape=(None,encoded.shape[-1],), batch_size=1)
    else:
        esn_input = Input(shape=(None,encoded.shape[-1],))

    esn_x_output,st = esn_layer(esn_input) 
    esn_cell = Model(esn_input,outputs=[esn_x_output,st])
    esn_y_output = ae_esn.get_layer('Wout')(esn_x_output)
    esn = Model(esn_input,outputs=[esn_y_output,st])

    print(ae_esn.summary())
    print(decoder.summary())
    print(encoder.summary())
    print(autoencoder.summary())
    print(esn.summary())
    
    return ae_esn, ae_esn_st, encoder, decoder, autoencoder, esn_cell, esn
