N. A. K. Doan
n.a.k.doan@tudelft.nl

This folder contains the rachived related to the paper:
"Auto-Encoded Reservoir Computing for Turbulence Learning"
  Doan, Polifke & Magri
  ICCS2021

AE_ESN_model.py
  Module that contains the function/class for the Autoencoder ESN

ESN_tools.py
  Module that contain the implementation of the ESN

train_AE_ESN.py
  Main script to create and train the AE-ESN

retrain_AE_ESN_rho6.py
  Secondary script to continue the training of an existing AE-ESN
 
