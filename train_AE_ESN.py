import tensorflow as tf
tf.keras.backend.set_floatx('float32')

from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint,EarlyStopping
from tensorflow.keras.layers import Input
from ESN_tools import *

from tensorflow.keras.models import Model
from tensorflow.keras import backend as K

#from ae_esn_v2 import *
from AE_ESN_model import *

import time
import h5py
import numpy as np
# import matplotlib.pyplot as plt
import os
import datetime
from contextlib import redirect_stdout


gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

# tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=7168)])

## READ FILE
Re = 30.0
fln = '../Kolmogorov_Re' + str(Re) + '_T5000_DT001.h5'
print('Reading dataset from :' + fln)
hf = h5py.File(fln,'r')
Nx = 24
Nu = 2

x = np.array(hf.get('x'))
t = np.array(hf.get('t'))
u_all = np.zeros((Nx,Nx,len(t),Nu))
u_all[:,:,:,0] = np.array(hf.get('u_refined'))
if Nu==2:
    u_all[:,:,:,1] = np.array(hf.get('v_refined'))
u_all = np.transpose(u_all,[2,0,1,3])
hf.close()

## DEFINE TRAINING
Ntrain = 180000
Ntest = 20000
u_all = u_all[:,:,:,:].astype('float32')
u_all = u_all-np.mean(u_all,0)
if (Re>20.1):
    jump = 10
    u_all = u_all[np.arange(0,Ntrain+Ntest+2*jump,jump),:,:,:].astype('float32')
    Ntrain = int(Ntrain/jump)
    Ntest = int(Ntest/jump)

u_train = u_all[0:Ntrain+1,:,:,:].astype('float32')
u_test = u_all[Ntrain+1:Ntrain+Ntest+2,:,:,:].astype('float32')
u_all = u_all[0:Ntrain+Ntest+2,:,:,:].astype('float32')

u_all = np.reshape(u_all,(1,Ntrain+Ntest+2,Nx,Nx,Nu))
u_train = np.reshape(u_train,(1,Ntrain+1,Nx,Nx,Nu))
u_test = np.reshape(u_test,(1,Ntest+1,Nx,Nx,Nu))

pretrain_ae = True
train_ae_esn = False

## AE-ESN Configuration
lmb = 0.0 #1e-05
drop_rate = 0.001
features_layers = [32, 64, 128]
latent_dim = 192
## Remove the two commented lines?? - NAKD
#batch_size = Ntrain
#input_img = Input(shape = (None,Nx,Nx,Nu), batch_size=1) #Input(shape=(None,Nx,Nx,Nu), batch_shape=(batch_size,Nx,Nx,Nu))
act_fct = 'tanh'
resize_meth = 'bilinear'

## ESN properties
num_units = 1024
num_inputs = latent_dim
alpha = 1.0
rho = 0.6
degree =  3
sparseness = 1. - degree / (num_units - 1.)
sigma_in = 0.5
gamma = 0.0001
filter_window= (3,3)
batch_norm = False
stateful = True
rn_seed = 1
##
print('Creating ae-esn')
ae_esn, ae_esn_st, encoder, decoder, ae, esn_cell, esn = create_timedist_ae_esn_model(Nx,Nu,
                                                               num_units, num_inputs, alpha, rho, sparseness, sigma_in,
                                                               Nu, stateful,
                                                               features_layers, latent_dim,
                                                               resize_meth, filter_window=filter_window, 
                                                               act_fct=act_fct, batch_norm=batch_norm, 
                                                               drop_rate=drop_rate, lmb=lmb, rn_seed=rn_seed)

## DEFINE TRAINING
learning_rate = 0.0001

if (pretrain_ae):
    print('Pretraining Autoencoder')
    ae.compile(optimizer=Adam(lr=learning_rate),loss='mse')

    nb_epoch = 2000
    batch_size = 100
    learning_rate_list = [0.001, 0.0001, 0.00001]
    pat = 100

    train_ae_history = []
    val_ae_history = []
    tempfn = './temp_ae_r' + str(rho) + '_sin' + str(sigma_in)
    model_cb=ModelCheckpoint(tempfn, monitor='val_loss',save_best_only=True,verbose=0,save_weights_only=True)
    early_cb=EarlyStopping(monitor='val_loss', patience=pat,verbose=0)
    cb = [model_cb, early_cb]
    ae_esn.save_weights(tempfn)
    for i in range(len(learning_rate_list)):
        learning_rate = learning_rate_list[i]
        K.set_value(ae.optimizer.lr,learning_rate)
        hist0 = ae.fit(u_train[0,:,:,:,:], u_train[0,:,:,:,:],
                    epochs=nb_epoch,
                    batch_size=batch_size,
                    shuffle=True,
                    validation_data=(u_test[0,:,:,:,:], u_test[0,:,:,:,:]),
                    callbacks=cb,
                    verbose=2)
        ae.load_weights(tempfn)
        train_ae_history.extend( hist0.history['loss'] )
        val_ae_history.extend( hist0.history['val_loss'] )


    ##  pretraining of the ESN
    print('Pretraining of ESN')
    Y_coded = encoder.predict(u_test[0,:,:,:,:])
    #Y_coded = Y_coded.reshape(1,Y_coded.shape[0],Y_coded.shape[1])
    Y_coded = Y_coded.reshape((1,Y_coded.shape[0],Y_coded.shape[1]))
    XE_coded = esn_cell.predict(Y_coded)
    XE_coded = XE_coded[0] 
    washout = 1000
    Y_coded = Y_coded[0,1+washout:,:]
    XE_coded = XE_coded[0,washout:-1,:]
    XE_coded = np.hstack((XE_coded,np.ones((XE_coded.shape[0],1))))
    Wout = np.matmul(np.linalg.inv(np.matmul(XE_coded.T,XE_coded)+gamma*np.eye(num_units+1)),np.matmul(XE_coded.T,Y_coded)   )
    K.set_value(ae_esn.get_layer('Wout').layer.kernel,Wout[:-1,:])
    K.set_value(ae_esn.get_layer('Wout').layer.bias,Wout[-1,:])
    print('FINISHED PRETRAINING AE AND ESN')

## DEFINE TRAINING
if (train_ae_esn):
    ae_esn.reset_states()
    print('Training AE-ESN')
    ae_esn.compile(optimizer=Adam(lr=learning_rate),loss='mse')
    batch_size = 2000
    nb_batch = int(Ntrain/batch_size)
    nb_batch_test = int(Ntest/batch_size)
    y_train = u_train[:,1:,:,:,:]
    x_train = u_train[:,:Ntrain,:,:,:]
    y_test = u_test[:,1:,:,:,:]
    x_test = u_test[:,:Ntest,:,:,:]
    x_train = x_train.reshape(nb_batch,batch_size,Nx,Nx,Nu)
    y_train = y_train.reshape(nb_batch,batch_size,Nx,Nx,Nu)
    x_test = x_test.reshape(nb_batch_test,batch_size,Nx,Nx,Nu)
    y_test = y_test.reshape(nb_batch_test,batch_size,Nx,Nx,Nu)

    nb_epoch = 2000
    learning_rate_list = [0.0001]
    pat = 100
    tempfn ='./temp_ae_esn_r' + str(rho) + '_sin' + str(sigma_in)
    train_history = []
    val_history = []
    best_val = 1e9
    no_improv = 0
    ae_esn.save_weights(tempfn)
    for i in range(len(learning_rate_list)):
        learning_rate = learning_rate_list[i]
        K.set_value(ae_esn.optimizer.lr,learning_rate)
        for epoch in range(nb_epoch):
            time0 = time.time()
            tr_batch_loss = []
            ae_esn.reset_states()
            for ibatch in range(nb_batch):
                tt_loss = ae_esn.train_on_batch(x_train[ibatch:ibatch+1,:,:,:,:],
                    y_train[ibatch:ibatch+1,:,:,:,:])
                tr_batch_loss.append(tt_loss)

            train_history.append(np.mean(tr_batch_loss))

            test_batch_loss = []
            for ibatch in range(nb_batch_test):
                tt_loss = ae_esn.test_on_batch(x_test[ibatch:ibatch+1,:,:,:,:],
                    y_test[ibatch:ibatch+1,:,:,:,:])
                test_batch_loss.append(tt_loss)
            
            val_history.append(np.mean(test_batch_loss))
            print('Epoch', epoch+1,'/',nb_epoch)
            print(round(time.time()-time0), 's - Training loss:', train_history[-1], ', Validation loss:', val_history[-1])

            if (val_history[-1]<best_val):
                print('Better model found, saving to: ', tempfn)
                best_val = val_history[-1]
                ae_esn.save_weights(tempfn)
                no_improv = 0
            else:
                no_improv = no_improv + 1

            if (no_improv>pat):
                ae_esn.load_weights(tempfn)
                break



    # model_cb=ModelCheckpoint(tempfn, monitor='val_loss',save_best_only=True,verbose=0,save_weights_only=True)
    # early_cb=EarlyStopping(monitor='val_loss', patience=pat,verbose=0)

    # train_history = []
    # val_history = []
    # for i in range(len(learning_rate_list)):
    #     learning_rate = learning_rate_list[i]
    #     K.set_value(ae_esn.optimizer.lr,learning_rate)
    #     hist0 = ae_esn.fit(u_train, y_train,
    #                 epochs=nb_epoch,
    #                 batch_size=batch_size,
    #                 shuffle=False,
    #                 validation_data=(u_test, y_test),
    #                 callbacks=cb,
    #                 verbose=2)

    #     train_history.extend( hist0.history['loss'] )
    #     val_history.extend( hist0.history['val_loss'] )


## PREDICT NETWORK
print('Doing final training prediction')
ae_esn.reset_states()
batch_size = 2000
y_train = np.zeros((1,Ntrain,Nx,Nx,Nu))
for i in range(int(Ntrain/batch_size)):
    y_train[:,i*batch_size:(i+1)*batch_size,:,:,:] = ae_esn.predict(u_train[:,i*batch_size:(i+1)*batch_size,:,:,:])
#y_train = ae_esn.predict(u_train)
print('Doing final validation prediction')
ae_esn.reset_states()
y_test = np.zeros((1,Ntest,Nx,Nx,Nu))
for i in range(int(Ntest/batch_size)):
    y_test[:,i*batch_size:(i+1)*batch_size,:,:,:] = ae_esn.predict(u_test[:,i*batch_size:(i+1)*batch_size,:,:,:])
#y_test = ae_esn.predict(u_test)
print('Doing all validation')
ae_esn.reset_states()
y_all = np.zeros((1,Ntrain+Ntest,Nx,Nx,Nu))
for i in range(int((Ntrain+Ntest)/batch_size)):
    y_all[:,i*batch_size:(i+1)*batch_size,:,:,:] = ae_esn.predict(u_all[:,i*batch_size:(i+1)*batch_size,:,:,:])
#y_all = ae_esn.predict(u_all)
coded_all = encoder.predict(u_all[0,:,:,:,:])
coded_all = coded_all.reshape((1,) + coded_all.shape)

ae_esn.reset_states()
uESNc_all,_ = esn.predict(coded_all) 

## autonomous prediction
print('Doing autonomous prediction')
ae_esn.reset_states()
washout = 1000
y_auto = ae_esn.predict(u_test[:,0:washout,:,:,:]) # initialization of the ae-esn

u_pred_auto = []
u_auto = y_auto[0:,-1:,:,:,:]
horizon = 5000
for i in range(horizon):
    if (i%50==0):
        print(i)
    u_auto = ae_esn.predict(u_auto)
    u_pred_auto.append(u_auto)

u_pred_auto = np.array(u_pred_auto)
u_pred_auto = np.squeeze(u_pred_auto)

## SAVE FILES
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d__%H_%M_%S')
fld = './Re' + str(Re) + '/result_' + st + '_model_dropout' + str(drop_rate) + '_' + act_fct + '_' + resize_meth +'_ESN/'
os.mkdir(fld)
fln = fld + 'Kol_ae_model'
ae.save_weights(fln)
fln = fld + 'Kol_ae_esn_model'
ae_esn.save_weights(fln)
fln = fld + 'Kol_encoder_model'
encoder.save_weights(fln)
fln = fld + 'Kol_decoder_model'
decoder.save_weights(fln)

    
fln = fld + 'Kol_ae_esn_summary.txt'
with open(fln, 'w') as f:
    with redirect_stdout(f):
        ae_esn.summary()
        
fln = fld + 'Kol_Model_MetaParam.h5'
hf = h5py.File(fln,'w')
hf.create_dataset('Nx',data=Nx)
hf.create_dataset('Nu',data=Nu)
hf.create_dataset('num_units',data=num_units)
hf.create_dataset('num_inputs',data=num_inputs)

hf.create_dataset('alpha',data=alpha)
hf.create_dataset('rho',data=rho)
hf.create_dataset('sparseness',data=sparseness)
hf.create_dataset('sigma_in',data=sigma_in)
hf.create_dataset('gamma',data=gamma)

hf.create_dataset('features_layers',data=features_layers)
hf.create_dataset('latent_dim',data=latent_dim)
hf.create_dataset('resize_meth',data=np.string_(resize_meth),dtype="S10")
hf.create_dataset('filter_window',data=filter_window)
hf.create_dataset('act_fct',data=np.string_(act_fct),dtype="S10")
hf.create_dataset('batch_norm',data=np.bool(batch_norm))
hf.create_dataset('drop_rate',data=drop_rate)
hf.create_dataset('lmb',data=lmb)
hf.create_dataset('stateful',data=np.bool(stateful))
hf.create_dataset('rn_seed',data=rn_seed)
hf.close()


fln = fld + 'Kol_results.h5'
hf = h5py.File(fln,'w')
hf.create_dataset('uNN',data=y_all[:,np.arange(0,Ntrain+Ntest,5),:,:,:])
hf.create_dataset('udata',data=u_all[:,np.arange(0,Ntrain+Ntest,5),:,:,:])
hf.create_dataset('ucNN',data=coded_all)
hf.create_dataset('uESNc',data=uESNc_all)
if (pretrain_ae):
    hf.create_dataset('train_ae_hist',data=np.array(train_ae_history))
    hf.create_dataset('valid_ae_hist',data=np.array(val_ae_history))

if (train_ae_esn):
    hf.create_dataset('train_hist',data=np.array(train_history))
    hf.create_dataset('valid_hist',data=np.array(val_history))
hf.create_dataset('upred',data=u_pred_auto)
hf.create_dataset('target',data=u_test[0,washout:washout+horizon,:,:,:])
hf.create_dataset('latent_dim',data=latent_dim)
hf.close()
# ## TEST STATEFULNESS
# Win = ae_esn_st.get_layer('ESN').variables[1].numpy()
# W = ae_esn_st.get_layer('ESN').variables[0].numpy()
# XX = ae_esn_st.get_layer('ESN').states
# XX = XX[0].numpy()

# UU = u_test[:,0:1,:,:,:]                                      
# UUESN = encoder.predict(UU[0,:,:,:,:])
# UUESN = UUESN.reshape((1,1,128))
# XXn = np.tanh(np.dot(XX,W) + np.dot(UUESN.reshape(1,128),Win))
# YY,XXn_predict = ae_esn_st.predict(UU)

