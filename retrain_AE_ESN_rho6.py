import tensorflow as tf
tf.keras.backend.set_floatx('float32')

from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint,EarlyStopping

from tensorflow.keras.models import Model, load_model
from tensorflow.keras import backend as K

#from autoencoder_v2 import *
#from timedist_AE_v2 import *
from AE_ESN_model import *

from ESN_tools import *

import time
import h5py
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime
from contextlib import redirect_stdout

gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

# tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=8000)])


## READ FILE
Re = 30.0
fln = '../Kolmogorov_Re' + str(Re) + '_T5000_DT001.h5'
print('Reading dataset from :' + fln)
hf = h5py.File(fln,'r')
Nx = 24
Nu = 2

x = np.array(hf.get('x'))
t = np.array(hf.get('t'))
u_all = np.zeros((Nx,Nx,len(t),Nu))
u_all[:,:,:,0] = np.array(hf.get('u_refined'))
if Nu==2:
    u_all[:,:,:,1] = np.array(hf.get('v_refined'))
u_all = np.transpose(u_all,[2,0,1,3])
hf.close()

## DEFINE TRAINING
Ntrain = 360000 # 180000
Ntest = 40000 #20000
u_all = u_all[:,:,:,:].astype('float32')
u_all = u_all-np.mean(u_all,0)
if (Re>20.1):
    jump = 10
    u_all = u_all[np.arange(0,Ntrain+Ntest+2*jump,jump),:,:,:].astype('float32')
    Ntrain = int(Ntrain/jump)
    Ntest = int(Ntest/jump)

u_train = u_all[0:Ntrain+1,:,:,:].astype('float32')
u_test = u_all[Ntrain+1:Ntrain+Ntest+2,:,:,:].astype('float32')
u_all = u_all[0:Ntrain+Ntest+2,:,:,:].astype('float32')
#del u_all
u_all = np.reshape(u_all,(1,Ntrain+Ntest+2,Nx,Nx,Nu))
u_train = np.reshape(u_train,(1,Ntrain+1,Nx,Nx,Nu))
u_test = np.reshape(u_test,(1,Ntest+1,Nx,Nx,Nu))

retrain_esn = False
retrain_ae = False
retrain_ae_esn = True

## DEFINE NETWORK
# fld0 = './'
fld0 = './Re' + str(Re) + '/result_2020_07_13__11_37_27_model_dropout0.001_tanh_bilinear_ESN/'
fln = fld0 + 'Kol_Model_MetaParam.h5'
print('Reading network metaparameters from: ' + fln)

hf = h5py.File(fln,'r')
Nx = np.array(hf.get('Nx'))
Nu = np.array(hf.get('Nu'))
num_units = hf.get('num_units')[()]
num_inputs = hf.get('num_inputs')[()]

alpha = np.array(hf.get('alpha'))
rho = np.array(hf.get('rho'))
sparseness = np.array(hf.get('sparseness'))
sigma_in = np.array(hf.get('sigma_in'))
gamma = np.array(hf.get('gamma'))

features_layers = np.array(hf.get('features_layers'))
latent_dim = np.array(hf.get('latent_dim'))
resize_meth = hf.get('resize_meth')[()].decode()
filter_window = np.array(hf.get('filter_window'))
act_fct = hf.get('act_fct')[()].decode()
batch_norm = hf.get('batch_norm')[()]
drop_rate = np.array(hf.get('drop_rate')).tolist()
lmb = np.array(hf.get('lmb'))
stateful = hf.get('stateful')[()]
rn_seed = np.array(hf.get('rn_seed'))
hf.close()

##  
print('Re-creating network')
ae_esn, ae_esn_st, encoder, decoder, ae, esn_cell, esn = create_timedist_ae_esn_model(Nx,Nu,
                                                               num_units, num_inputs, alpha, rho, sparseness, sigma_in,
                                                               Nu, stateful,
                                                               features_layers, latent_dim,
                                                               resize_meth, filter_window=filter_window, 
                                                               act_fct=act_fct, batch_norm=batch_norm, 
                                                               drop_rate=drop_rate, lmb=lmb, rn_seed=rn_seed)


## DEFINE NETWORK
print('Loading weights for network')
fld = fld0 + 'result_2020_09_02__07_59_08_model/' #'result_2020_08_31__11_32_11_model/' #'result_2020_08_28__20_13_10_model/'# 'result_2020_08_27__12_24_18_model/' #'result_2020_08_25__14_37_34_model/'# 'result_2020_08_24__11_08_10_model/' #'result_2020_08_21__17_12_05_model/' #'result_2020_07_13__21_35_31_model/' 
fln = 'Kol_ae_esn_model'
#fln = 'temp_autoencoder.h5'
#ae_esn = load_model(fld0 + fln, custom_objects={'ResizeImages':ResizeImages, 'ESNCell':ESNCell})
ae_esn.load_weights(fld+fln) 

fln = 'Kol_encoder_model'
encoder.load_weights(fld + fln)

fln = 'Kol_decoder_model'
decoder.load_weights(fld + fln)

fln = 'Kol_ae_model'
ae.load_weights(fld + fln)

## DEFINE TRAINING OF AE
if (retrain_ae):
    print('Re-training of Autoencoder')
    learning_rate = 0.0001
    ae.compile(optimizer=Adam(lr=learning_rate),loss='mse')

    nb_epoch = 2
    batch_size = 100 
    learning_rate_list = [0.0001] #[0.001, 0.0001, 0.00001]
    pat = 100

    train_ae_history = []
    val_ae_history = []
    # tempfn = './temp_autoencoder'
    tempfn = './temp_ae_r' + str(rho) + '_sin' + str(sigma_in)
    model_cb=ModelCheckpoint(tempfn, monitor='val_loss',save_best_only=True,verbose=0,save_weights_only=True)
    early_cb=EarlyStopping(monitor='val_loss', patience=pat,verbose=0)
    cb = [model_cb, early_cb]
    ae.save_weights(tempfn)
    for i in range(len(learning_rate_list)):
        learning_rate = learning_rate_list[i]
        K.set_value(ae.optimizer.lr,learning_rate)
        hist0 = ae.fit(u_train[0,:,:,:,:], u_train[0,:,:,:,:],
                epochs=nb_epoch,
                batch_size=batch_size,
                shuffle=True,
                validation_data=(u_test[0,:,:,:,:], u_test[0,:,:,:,:]),
                callbacks=cb,
		verbose=2)
        ae.load_weights(tempfn)
        train_ae_history.extend( hist0.history['loss'] )
        val_ae_history.extend( hist0.history['val_loss'] )
    retrain_esn = True

if (retrain_esn):
    print('Re-training of ESN')
    ae_esn.reset_states()
    gamma = 0.00000001
    Y_coded = encoder.predict(u_test[0,:,:,:,:])
    Y_coded = Y_coded.reshape((1,Y_coded.shape[0],Y_coded.shape[1]))
    XE_coded = esn_cell.predict(Y_coded)
    XE_coded = XE_coded[0]
    washout = 1000
    Y_coded = Y_coded[0,1+washout:,:]
    XE_coded = XE_coded[0,washout:-1,:]
    XE_coded = np.hstack((XE_coded,np.ones((XE_coded.shape[0],1))))
    Wout = np.matmul(np.linalg.inv(np.matmul(XE_coded.T,XE_coded)+gamma*np.eye(num_units+1)),np.matmul(XE_coded.T,Y_coded)   )
    K.set_value(ae_esn.get_layer('Wout').layer.kernel,Wout[:-1,:])
    K.set_value(ae_esn.get_layer('Wout').layer.bias,Wout[-1,:])

## DEFINE TRAINING OF AE-ESN
if (retrain_ae_esn):
    ae_esn.reset_states()
    print('Re-training of AE-ESN')
    nb_epoch = 2000
    #batch_size = 1
    train_history = []
    val_history = []
    learning_rate_list = [0.000001] #[0.000001] #, 0.0001, 0.00001]
    print('Training AE-ESN')
    ae_esn.compile(optimizer=Adam(lr=0.0001),loss='mse')
    batch_size = 4000
    nb_batch = int(Ntrain/batch_size)
    nb_batch_test = int(Ntest/batch_size)
    y_train = u_train[:,1:,:,:,:]
    x_train = u_train[:,:Ntrain,:,:,:]
    y_test = u_test[:,1:,:,:,:]
    x_test = u_test[:,:Ntest,:,:,:]
    x_train = x_train.reshape(nb_batch,batch_size,Nx,Nx,Nu)
    y_train = y_train.reshape(nb_batch,batch_size,Nx,Nx,Nu)
    x_test = x_test.reshape(nb_batch_test,batch_size,Nx,Nx,Nu)
    y_test = y_test.reshape(nb_batch_test,batch_size,Nx,Nx,Nu)

    #nb_epoch =  2000
    #learning_rate_list = [0.0001]
    pat = 100
    # tempfn = './temp_ae_esn'
    tempfn ='./temp_ae_esn_r' + str(rho) + '_sin' + str(sigma_in)
    train_history = []
    val_history = []
    best_val = 1e9
    no_improv = 0
    ae_esn.save_weights(tempfn)
    for i in range(len(learning_rate_list)):
        learning_rate = learning_rate_list[i]
        K.set_value(ae_esn.optimizer.lr,learning_rate)
        for epoch in range(nb_epoch):
            time0 = time.time()
            tr_batch_loss = []
            ae_esn.reset_states()
            for ibatch in range(nb_batch):
                tt_loss = ae_esn.train_on_batch(x_train[ibatch:ibatch+1,:,:,:,:],
                    y_train[ibatch:ibatch+1,:,:,:,:])
                tr_batch_loss.append(tt_loss)

            train_history.append(np.mean(tr_batch_loss))

            test_batch_loss = []
            for ibatch in range(nb_batch_test):
                tt_loss = ae_esn.test_on_batch(x_test[ibatch:ibatch+1,:,:,:,:],
                    y_test[ibatch:ibatch+1,:,:,:,:])
                test_batch_loss.append(tt_loss)
            
            val_history.append(np.mean(test_batch_loss))
            print('Epoch', epoch+1,'/',nb_epoch)
            print(round(time.time()-time0), 's - Training loss:', train_history[-1], ', Validation loss:', val_history[-1])

            if (val_history[-1]<best_val):
                print('Better model found, saving to: ', tempfn)
                best_val = val_history[-1]
                ae_esn.save_weights(tempfn)
                no_improv = 0
            else:
                no_improv = no_improv + 1

            if (no_improv>pat):
                ae_esn.load_weights(tempfn)
                break




    # pat = 100
    # tempfn = './temp_ae_esn'
    # model_cb=ModelCheckpoint(tempfn, monitor='val_loss',save_best_only=True,verbose=1, save_weights_only=True)
    # early_cb=EarlyStopping(monitor='val_loss', patience=pat,verbose=1)
    # cb = [model_cb, early_cb]
    # ae_esn.compile(optimizer=Adam(lr=0.0001),loss='mse')
    # for i in range(len(learning_rate_list)):
    #     learning_rate = learning_rate_list[i]
    #     K.set_value(ae_esn.optimizer.lr,learning_rate)
    #     hist0 = ae_esn.fit(u_train[:,:-1,:,:,:], u_train[:,1:,:,:,:],
    #             epochs=nb_epoch,
    #             batch_size=batch_size,
    #             shuffle=False,
    #             validation_data=(u_test[:,:-1,:,:,:], u_test[:,1:,:,:,:]),
    #             callbacks=cb)

    #     train_history.extend( hist0.history['loss'] )
    #     val_history.extend( hist0.history['val_loss'] )

## PREDICT NETWORK
print('Doing final training prediction')
batch_size = 2000
y_train = np.zeros((1,Ntrain,Nx,Nx,Nu))
for i in range(int(Ntrain/batch_size)):
    y_train[:,i*batch_size:(i+1)*batch_size,:,:,:] = ae_esn.predict(u_train[:,i*batch_size:(i+1)*batch_size,:,:,:])
#y_train = ae_esn.predict(u_train)
print('Doing final validation prediction')
y_test = np.zeros((1,Ntest,Nx,Nx,Nu))
for i in range(int(Ntest/batch_size)):
    y_test[:,i*batch_size:(i+1)*batch_size,:,:,:] = ae_esn.predict(u_test[:,i*batch_size:(i+1)*batch_size,:,:,:])
#y_test = ae_esn.predict(u_test)
print('Doing all validation')
y_all = np.zeros((1,Ntrain+Ntest,Nx,Nx,Nu))
for i in range(int((Ntrain+Ntest)/batch_size)):
    y_all[:,i*batch_size:(i+1)*batch_size,:,:,:] = ae_esn.predict(u_all[:,i*batch_size:(i+1)*batch_size,:,:,:])

#coded_all = encoder.predict(u_all)

## autonomous prediction
washout = 1000
y_auto = ae_esn.predict(u_test[:,0:washout,:,:,:]) # initialization of the ae-esn

u_pred_auto = []
u_auto = y_auto[0:,-1:,:,:,:]
horizon = 5000
for i in range(horizon):
    if (i%50==0):
        print(i)
    u_auto = ae_esn.predict(u_auto)
    u_pred_auto.append(u_auto)

u_pred_auto = np.array(u_pred_auto)
u_pred_auto = np.squeeze(u_pred_auto)


## SAVE FILES
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d__%H_%M_%S')
fld = fld0 + '/result_' + st + '_model/'
print('Saving in ', fld)
os.mkdir(fld)
fln = fld + 'Kol_ae_model'
ae.save_weights(fln)
fln = fld + 'Kol_ae_esn_model'
ae_esn.save_weights(fln)
fln = fld + 'Kol_encoder_model'
encoder.save_weights(fln)
fln = fld + 'Kol_decoder_model'
decoder.save_weights(fln)

fln = fld + 'Kol_ae_esn_summary.txt'
with open(fln, 'w') as f:
    with redirect_stdout(f):
        ae_esn.summary()

fln = fld + 'Kol_results.h5'
hf = h5py.File(fln,'w')
#hf.create_dataset('udata',data=u_all)
hf.create_dataset('uNN',data=y_all[:,np.arange(0,Ntrain+Ntest,5),:,:,:])
hf.create_dataset('upred',data=u_pred_auto)
hf.create_dataset('target',data=u_test[0,washout:washout+horizon,:,:,:])
hf.create_dataset('latent_dim',data=latent_dim)
#hf.create_dataset('ucNN',data=coded_all)
if (retrain_ae_esn):
    hf.create_dataset('train_hist',data=np.array(train_history))
    hf.create_dataset('valid_hist',data=np.array(val_history))
if (retrain_ae):
    hf.create_dataset('train_ae_hist',data=np.array(train_ae_history))
    hf.create_dataset('valid_ae_hist',data=np.array(val_ae_history))

hf.close()










